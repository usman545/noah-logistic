<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/news', 'HomeController@news')->name('news');
Route::get('/services', 'HomeController@services')->name('services');
Route::get('/faqs', 'HomeController@faqs')->name('faqs');
Route::get('/locations', 'HomeController@locations')->name('locations');
