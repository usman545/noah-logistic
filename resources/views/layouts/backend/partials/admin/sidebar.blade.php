<header class="main-header">
    <a href="{{route('dashboard')}}" class="logo">
        <span class="logo-mini"><b>SAI</b></span>
        <span class="logo-lg"><b>{{ config('app.name', 'Satiya Sai') }}</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="javascript:void(0)" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('uploads/users/'.Auth::user()->picture)}}"
                             class="user-image" alt="User Image">
                        <span class="hidden-xs">{{Auth::user()->first_name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{asset('uploads/users/'.Auth::user()->picture)}}"
                                 class="img-circle" alt="User Image">
                            <p>
                                {{Auth::user()->fname}}
                                <small>Member {{Auth::user()->created_at->diffForHumans()}}</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('dashboard')}}" class="btn btn-default btn-flat">Dashboard</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Log out</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('uploads/users/'.Auth::user()->picture)}}" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->first_name}}</p>
                <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="@if(Request::segment(2)=='view-slider' || Request::segment(2)=='add-slider'){{'active'}}@endif">
                <a href="{{route('system.admin.view-slider')}}">
                    <i class="fa fa-sliders"></i> <span>Sliders</span>
                </a>
            </li>
            <li class="@if(Request::segment(2)=='view-team' || Request::segment(2)=='add-team'){{'active'}}@endif">
                <a href="{{route('system.admin.view-team')}}">
                    <i class="fa fa-users"></i> <span>Team</span>
                </a>
            </li>
            <li class="@if(Request::segment(2)=='view-about-us' || Request::segment(2)=='add-about-us'){{'active'}}@endif">
                <a href="{{route('system.admin.view.about.us')}}">
                    <i class="fa fa-building"></i> <span>About Us</span>
                </a>
            </li>
            <li class="@if(Request::segment(2)=='view-events' || Request::segment(2)=='add-events'){{'active'}}@endif">
                <a href="{{route('system.admin.view.events')}}">
                    <i class="fa fa-map"></i> <span>Events</span>
                </a>
            </li>
            <li class="@if(Request::segment(2)=='view-service' || Request::segment(2)=='add-service'){{'active'}}@endif">
                <a href="{{route('system.admin.view.service')}}">
                    <i class="fa fa-tasks"></i> <span>Services</span>
                </a>
            </li>
            <li class="@if(Request::segment(2)=='view-portfolio' || Request::segment(2)=='add-portfolio'){{'active'}}@endif">
                <a href="{{route('system.admin.view.portfolio')}}">
                    <i class="fa fa-user"></i> <span>Portfolio</span>
                </a>
            </li>
            @if (Auth::user()->type == "admin")
            <li class="@if(Request::segment(2)=='view-users' || Request::segment(2)=='add-users'){{'active'}}@endif">
                <a href="{{route('system.admin.view.users')}}">
                    <i class="fa fa-tasks"></i> <span>Users</span>
                </a>
            </li>
            @endif
            <li class="treeview {{(Request::segment(2)=='home-page-setting' ||  Request::segment(2)=='about-us-page-setting' ||  Request::segment(2)=='portfolio-page-setting' ||  Request::segment(2)=='team-page-setting' ||  Request::segment(2)=='service-page-setting' ||  Request::segment(2)=='blog-page-setting' ||  Request::segment(2)=='contact-page-setting')?'menu-open':''}}">
                <a href="javascript:void(0)">
                    <i class="fa fa-file-code-o"></i>
                    <span>Pages Setting</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="{{(Request::segment(2)=='home-page-setting' ||  Request::segment(2)=='about-us-page-setting' ||  Request::segment(2)=='portfolio-page-setting' ||  Request::segment(2)=='team-page-setting' ||  Request::segment(2)=='service-page-setting' ||  Request::segment(2)=='blog-page-setting' ||  Request::segment(2)=='contact-page-setting')?'display:block':'display:none;'}}">
                    <li class="{{(Request::segment(2)=='home-page-setting')?'active':''}}"><a href="{{route('system.admin.home.page.settings')}}"><i class="fa fa-circle-o"></i> Banner Settings</a></li>
                    <li class="{{(Request::segment(2)=='home-page-social')?'active':''}}"><a href="{{route('system.admin.add-social')}}"><i class="fa fa-circle-o"></i> Social Settings</a></li>
                    <!-- <li class="{{(Request::segment(2)=='about-us-page-setting')?'active':''}}"><a href="{{route('system.admin.about.page.settings')}}"><i class="fa fa-circle-o"></i> About Page</a></li>
                    <li class="{{(Request::segment(2)=='portfolio-page-setting')?'active':''}}"><a href="{{route('system.admin.portfolio.page.settings')}}"><i class="fa fa-circle-o"></i> Portfolio Page</a></li>
                    <li class="{{(Request::segment(2)=='team-page-setting')?'active':''}}"><a href="{{route('system.admin.team.page.settings')}}"><i class="fa fa-circle-o"></i> Team Page</a></li>
                    <li class="{{(Request::segment(2)=='service-page-setting')?'active':''}}"><a href="{{route('system.admin.service.page.settings')}}"><i class="fa fa-circle-o"></i> Services Page</a></li>
                    <li class="{{(Request::segment(2)=='blog-page-setting')?'active':''}}"><a href="{{route('system.admin.blog.page.settings')}}"><i class="fa fa-circle-o"></i> Blog Page</a></li>
                    <li class="{{(Request::segment(2)=='contact-page-setting')?'active':''}}"><a href="{{route('system.admin.contact.page.settings')}}"><i class="fa fa-circle-o"></i> Contact Page</a></li> -->
                </ul>
            </li>
            <li>
                <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> <span>Log out</span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </a>
            </li>

            {{--<li class="treeview {{(Request::segment(2)=='two-level-category' || Request::segment(2)=='view-two-level-categories')?'menu-open':''}}">--}}
                {{--<a href="javascript:void(0)">--}}
                    {{--<i class="fa fa-bars"></i>--}}
                    {{--<span>Two Level Categories</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="{{(Request::segment(2)=='two-level-category' || Request::segment(2)=='view-two-level-categories')?'display:block':'display:none;'}}">--}}
                    {{--<li class="{{(Request::segment(2)=='two-level-category')?'active':''}}"><a href="{{route('admin.two.level.category')}}"><i class="fa fa-circle-o"></i> Add Two Level Category</a></li>--}}
                    {{--<li class="{{(Request::segment(2)=='view-two-level-categories')?'active':''}}"><a href="{{route('admin.view.two.level.category')}}"><i class="fa fa-circle-o"></i> View Two Level Categories</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="treeview {{(Request::segment(2)=='three-level-category' || Request::segment(2)=='view-three-level-categories')?'menu-open':''}}">--}}
                {{--<a href="javascript:void(0)">--}}
                    {{--<i class="fa fa-bars"></i>--}}
                    {{--<span>Three Level Categories</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="{{(Request::segment(2)=='three-level-category' || Request::segment(2)=='view-three-level-categories')?'display:block':'display:none;'}}">--}}
                    {{--<li class="{{(Request::segment(2)=='three-level-category')?'active':''}}"><a href="{{route('admin.three.level.category')}}"><i class="fa fa-circle-o"></i> Add Three Level Category</a></li>--}}
                    {{--<li class="{{(Request::segment(2)=='view-three-level-categories')?'active':''}}"><a href="{{route('admin.view.three.level.category')}}"><i class="fa fa-circle-o"></i> View Three Level Categories</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="@if(Request::segment(2)=='view-news' || Request::segment(2)=='add-news'){{'active'}}@endif">--}}
                {{--<a href="{{route('admin.view-news')}}">--}}
                    {{--<i class="fa fa-map"></i> <span>News</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="treeview {{(Request::segment(2)=='view-news' || Request::segment(2)=='add-news')?'menu-open':''}}">--}}
                {{--<a href="javascript:void(0)">--}}
                    {{--<i class="fa fa-map"></i>--}}
                    {{--<span>News</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="{{(Request::segment(2)=='view-news' || Request::segment(2)=='add-news')?'display:block':'display:none;'}}">--}}
                    {{--<li class="{{(Request::segment(2)=='add-news')?'active':''}}"><a href="{{route('admin.add-news')}}"><i class="fa fa-circle-o"></i> Add News</a></li>--}}
                    {{--<li class="{{(Request::segment(2)=='view-news')?'active':''}}"><a href="{{route('admin.view-news')}}"><i class="fa fa-circle-o"></i> View News</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="@if(Request::segment(2)=='view-faq' || Request::segment(2)=='add-faq'){{'active'}}@endif">--}}
                {{--<a href="{{route('admin.view-faq')}}">--}}
                    {{--<i class="fa fa-circle-thin"></i> <span>Faq</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="treeview {{(Request::segment(2)=='view-faq' || Request::segment(2)=='add-faq')?'menu-open':''}}">--}}
                {{--<a href="javascript:void(0)">--}}
                    {{--<i class="fa fa-circle-thin"></i>--}}
                    {{--<span>Manage Faq</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="{{(Request::segment(2)=='view-faq' || Request::segment(2)=='add-faq')?'display:block':'display:none;'}}">--}}
                    {{--<li class="{{(Request::segment(2)=='add-faq')?'active':''}}"><a href="{{route('admin.add-faq')}}"><i class="fa fa-circle-o"></i> Add Faq</a></li>--}}
                    {{--<li class="{{(Request::segment(2)=='view-faq')?'active':''}}"><a href="{{route('admin.view-faq')}}"><i class="fa fa-circle-o"></i> View Faq</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="@if(Request::segment(2)=='view-post' || Request::segment(2)=='add-post'){{'active'}}@endif">--}}
                {{--<a href="{{route('admin.view-post')}}">--}}
                    {{--<i class="fa fa-archive"></i> <span>Posts</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="@if(Request::segment(2)=='view-post-category' || Request::segment(2)=='add-post-category'){{'active'}}@endif">--}}
                {{--<a href="{{route('admin.view.post.category')}}">--}}
                    {{--<i class="fa fa-bars"></i> <span>Post Category</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            {{--<li class="treeview {{(Request::segment(2)=='view-post' || Request::segment(2)=='add-post')?'menu-open':''}}">--}}
                {{--<a href="javascript:void(0)">--}}
                    {{--<i class="fa fa-archive "></i>--}}
                    {{--<span>Manage Posts</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="{{(Request::segment(2)=='view-post' || Request::segment(2)=='add-post')?'display:block':'display:none;'}}">--}}
                    {{--<li class="{{(Request::segment(2)=='add-post')?'active':''}}"><a href="{{route('admin.add-post')}}"><i class="fa fa-circle-o"></i> Add Post</a></li>--}}
                    {{--<li class="{{(Request::segment(2)=='view-post')?'active':''}}"><a href="{{route('admin.view-post')}}"><i class="fa fa-circle-o"></i> View Post</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="@if(Request::segment(2)=='view-contact-us'){{'active'}}@endif">--}}
                {{--<a href="{{route('admin.view.contact.us')}}">--}}
                    {{--<i class="fa fa-envelope"></i> <span>Messages</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="@if(Request::segment(2)=='view-manufacturer' || Request::segment(2)=='add-manufacturer'){{'active'}}@endif">--}}
                {{--<a href="{{route('admin.view.manufacturer')}}">--}}
                    {{--<i class="fa fa-users"></i> <span>Manufacturer</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="treeview {{(Request::segment(2)=='view-manufacturer' || Request::segment(2)=='add-manufacturer')?'menu-open':''}}">--}}
                {{--<a href="javascript:void(0)">--}}
                    {{--<i class="fa fa-users"></i>--}}
                    {{--<span>Manage Manufacturer</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="{{(Request::segment(2)=='view-manufacturer' || Request::segment(2)=='add-manufacturer')?'display:block':'display:none;'}}">--}}
                    {{--<li class="{{(Request::segment(2)=='add-manufacturer')?'active':''}}"><a href="{{route('admin.add.manufacturer')}}"><i class="fa fa-circle-o"></i> Add Manufacturer</a></li>--}}
                    {{--<li class="{{(Request::segment(2)=='view-manufacturer')?'active':''}}"><a href="{{route('admin.view.manufacturer')}}"><i class="fa fa-circle-o"></i> View Manufacturer</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="@if(Request::segment(2)=='view-admin' || Request::segment(2)=='add-admin'){{'active'}}@endif">--}}
                {{--<a href="{{route('system.admin.view-admin')}}">--}}
                    {{--<i class="fa fa-users"></i> <span>Admin</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="treeview {{(Request::segment(3)=='view-slider' || Request::segment(3)=='add-slider')?'menu-open':''}}">--}}
                {{--<a href="javascript:void(0)">--}}
                    {{--<i class="fa fa-users"></i>--}}
                    {{--<span>Settings</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="{{(Request::segment(3)=='view-slider' || Request::segment(3)=='add-slider')?'display:block':'display:none;'}}">--}}
                    {{--<li class="{{(Request::segment(3)=='add-slider' || Request::segment(3)=='view-slider')?'active':''}}">--}}
                        {{--<a href="{{route('admin.view.slider')}}"><i class="fa fa-circle-o"></i> Slider</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </section>
</aside>