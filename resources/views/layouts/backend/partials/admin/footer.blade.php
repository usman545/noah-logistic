<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@if($page_name && $page_name=='loginpage')
    <script src="{{asset('bower_components/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
@elseif($page_name && $page_name=='admindashboard')
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <!-- Morris.js charts -->
    <script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'revenue-chart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                { year: '2008', value: 0 },
                { year: '2009', value: 10 },
                { year: '2010', value: 5 },
                { year: '2011', value: 5 },
                { year: '2012', value: 20 }
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'year',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>
@elseif(isset($page_name) && $page_name=='viewusers')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get.users') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "type"},
                    {"data": "first_name"},
                    {"data": "email"},
                    {"data": "picture"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
@elseif(isset($page_name) && $page_name=='adduser')
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script src="{{asset('bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@elseif(isset($page_name) && $page_name=='viewservices')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get.service') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "title"},
                    {"data": "views"},
                    {"data": "featured"},
                    {"data": "picture"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
@elseif(isset($page_name) && $page_name=='addservice')
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script src="{{asset('bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@elseif(isset($page_name) && $page_name=='addevent')
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script src="{{asset('bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@elseif(isset($page_name) && $page_name=='viewevents')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get.events') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "title"},
                    {"data": "views"},
                    {"data": "featured"},
                    {"data": "picture"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
@elseif(isset($page_name) && $page_name=='viewcontactus')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#item-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('admin.get.contact.us') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "email"},
                    {"data": "created_at"},
                    {"data": "status"},
                    {"data": "slug"}
                ]
            });
        });
        function viewEnquiry(item) {
            $('#view-enquiry').modal('show');
        }
    </script>
@elseif(isset($page_name) && $page_name=='viewslider')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get-slider') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "title"},
                    {"data": "picture"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
@elseif(isset($page_name) && $page_name=='addslider')
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script src="{{asset('bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@elseif(isset($page_name) && $page_name=='viewteam')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get-team') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "full_name"},
                    {"data": "position"},
                    {"data": "picture"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
@elseif(isset($page_name) && $page_name=='addsteam')
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script src="{{asset('bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    {{--<script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>--}}
@elseif(isset($page_name) && $page_name=='viewaboutus')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get.about.us') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "title"},
                    {"data": "picture"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
@elseif(isset($page_name) && $page_name=='addaboutus')
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script src="{{asset('bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@elseif(isset($page_name) && $page_name=='viewPortfolio')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get.portfolio') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "title"},
                    {"data": "views"},
                    {"data": "picture"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>

    @elseif(isset($page_name) && $page_name=='webHomePageSettings')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get-banners') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "page"},
                    {"data": "banner"},
                    {"data": "is_active"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
    @elseif(isset($page_name) && $page_name=='viewsocial')
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#items-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('system.admin.get-socials') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "facebook"},
                    {"data": "google"},
                    {"data": "twitter"},
                    {"data": "instagram"},
                    {"data": "linkedin"},
                    {"data": "created_at"},
                    {"data": "slug"}
                ]
            });
        });
    </script>
@elseif(isset($page_name) && $page_name=='addPortfolio')
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('bower_components/dist/js/demo.js')}}"></script>
    <script src="{{asset('bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script>
        $(function () {
            $('.textarea').wysihtml5()
        })
    </script>
@endif
