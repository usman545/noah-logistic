<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="name" description="Unique Solutions">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
    @include('layouts.backend.partials.admin.header',['page_name'=>(isset($page_name))?$page_name:''])
</head>
@yield('content')
<!-- Scripts -->
@include('layouts.backend.partials.admin.footer',['page_name'=>(isset($page_name))?$page_name:''])
</body>
</html>
