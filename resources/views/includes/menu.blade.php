<!-- MAIN NAVIGATION -->
<div class="header-inner">
    <!-- .container start -->
    <div class="container">
        <!-- .main-nav start -->
        <div class="main-nav">
            <!-- .row start -->
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default nav-left" role="navigation">

                        <!-- .navbar-header start -->
                        <div class="navbar-header">
                            <div class="logo">
                                <a href="index.html">
                                    <img src="{{asset('img/logo.jpg')}}" width="150px" alt="Trucking Transportation and Logistics HTML Template"/>
                                </a>
                            </div><!-- .logo end -->
                        </div><!-- .navbar-header start -->
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="">
                                    <a href="{{route('home')}}" >Home</a>

                                </li><!-- .dropdown end -->

                                <li class=" current-menu-item">
                                    <a href="{{route('about')}}" >About</a>
                                </li><!-- .dropdown end -->

                                <li class="dropdown">
                                    <a href="{{route('services')}}" data-toggle="dropdown" class="dropdown-toggle">Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="services01.html">Services overview</a></li>
                                        <li class="dropdown dropdown-submenu">
                                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Logistics</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="services02.html">Logistics</a></li>
                                                <li><a href="overland-transportation.html">Overland transportation</a></li>
                                                <li><a href="air-freight.html">Air freight</a></li>
                                                <li><a href="ocean-freight.html">Ocean freight</a></li>
                                                <li><a href="large-projects.html">Large projects</a></li>
                                                <li><a href="rail-transportation.html">Rail international shipping</a></li>
                                                <li><a href="contract-logistics.html">Contract logistics</a></li>
                                            </ul><!-- .dropdown-menu end -->
                                        </li><!-- .dropdown-submenu end -->
                                        <li><a href="warehousing.html">Warehousing</a></li>
                                        <li><a href="supply-chain-management.html">Supply chain management</a></li>
                                        <li><a href="packaging-options.html">Packaging options</a></li>
                                        <li><a href="consulting-services.html">Consulting services</a></li>
                                    </ul><!-- .dropdown-menu end -->
                                </li><!-- .dropdown end -->

                                <li class="">
                                    <a href="{{route('news')}}" >News</a>

                                </li><!-- .dropdown end -->


                                <li><a href="{{route('locations')}}">Locations</a></li>

                                <li class="">
                                    <a href="{{route('contact')}}" >Contact</a>

                                </li><!-- .dropdown end -->
                                <li class="dropdown">
                                    <a href="{{route('faqs')}}" >Faq</a>

                                </li><!-- .dropdown end -->

                            </ul><!-- .nav.navbar-nav end -->

                            <!-- RESPONSIVE MENU -->
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>

                                <ul class="dl-menu">
                                    <li>
                                        <a href="{{route('home')}}">Home</a>

                                    </li>

                                    <li>
                                        <a href="{{route('about')}}">About</a>

                                    </li>

                                    <li>
                                        <a href="{{route('services')}}">Services</a>
                                        <ul class="dl-submenu">
                                            <li><a href="services01.html">Services overview</a></li>
                                            <li>
                                                <a href="#">Logistics</a>
                                                <ul class="dl-submenu">
                                                    <li><a href="services02.html">Logistics</a></li>
                                                    <li><a href="overland-transportation.html">Overland transportation</a></li>
                                                    <li><a href="air-freight.html">Air freight</a></li>
                                                    <li><a href="ocean-freight.html">Ocean freight</a></li>
                                                    <li><a href="large-projects.html">Large projects</a></li>
                                                    <li><a href="rail-transportation.html">Rail international shipping</a></li>
                                                    <li><a href="contract-logistics.html">Contract logistics</a></li>
                                                </ul><!-- .dl-submenu end -->
                                            </li>
                                            <li><a href="warehousing.html">Warehousing</a></li>
                                            <li><a href="supply-chain-management.html">Supply chain management</a></li>
                                            <li><a href="packaging-options.html">Packaging options</a></li>
                                            <li><a href="consulting-services.html">Consulting services</a></li>
                                        </ul><!-- dl-submenu end -->
                                    </li>

                                    <li>
                                        <a href="{{route('news')}}">News</a>

                                    </li>


                                    <li><a href="{{route('locations')}}">Locations</a></li>


                                    <li>
                                        <a href="{{route('contact')}}">Contact</a>

                                    </li>
                                </ul><!-- .dl-menu end -->
                            </div><!-- #dl-menu end -->

                            <!-- #search start -->
                            
                        </div><!-- MAIN NAVIGATION END -->
                    </nav><!-- .navbar.navbar-default end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .main-nav end -->
    </div><!-- .container end -->
</div><!-- .header-inner end -->
