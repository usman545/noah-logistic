<div id="top-bar-wrapper" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-9">
                <ul id="quick-links" class="clearfix">
                    <li>
                        <i class="fa fa-phone"></i>
                        <span>416 619 4900</span>
                    </li>

                    <li>
                        <i class="fa fa-envelope"></i>
                        <a href="contact-simple.html">Contact us today</a>
                    </li>
                </ul><!-- .quick links end -->
            </div><!-- .col-md-6 end -->

            <div class="col-md-6 col-xs-3">
                <div class="wpml-languages enabled">
                    <a class="active" href="index08.html">
                        <img src="{{asset('img/en.png')}}" alt="English"/>

                        <i class="fa fa-chevron-down"></i>
                    </a>

                    <ul class="wpml-lang-dropdown">
                        <li>
                            <a href="index08.html">
                                <img src="{{ asset('img/de.png')}}" alt="German"/>
                            </a>
                        </li>

                        <li>
                            <a href="index08.html">
                                <img src="{{asset('img/it.png')}}" alt="Italian"/>
                            </a>
                        </li>
                    </ul><!-- .wpml-lang-dropdown end -->
                </div><!-- .wpml-languages.enabled end -->
            </div><!-- .col-md-6 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div><!-- .top-bar-wrapper end -->
