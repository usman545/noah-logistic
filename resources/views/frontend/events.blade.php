@extends('layouts.frontend.app')
@section('content')
    <!-- <div id="parallax2" class="parallax">
        <div class="bg2 parallax-bg"></div>
        <div class="overlay"></div>
        <div class="parallax-content">
            <div class="container">


            </div>
        </div>
    </div> -->
    <div class="page_banner">
        @if(!empty($banner))
        <img src="{{asset('/uploads/banner/'.$banner->banner)}}" >
        @endif
    </div>

    <div class="breadcrumbs1_wrapper">
        <div class="container">
            <div class="breadcrumbs1"><a href="/">Home</a><span>/</span>New & Events</div>
        </div>
    </div>

    <div class="container">
        <h3>News & Events</h3>
        <hr style="margin-top: -12px;">
        <div class="row">
            @isset($events)
                @foreach($events as $event)
                    <div class="col-md-3 col-sm-3 our-services">
                        <h4>{{(strlen($event->title)<=14)?$event->title:substr($event->title,0,12).'..'}}</h4>
                        <img src="{{asset('uploads/events/'.$event->picture)}}" alt="{{$event->title}}">
                        <p>{!! (strlen($event->description)<=103)?$event->description:substr($event->description,0,101).'..' !!}</p>
                        <a href="{{ url('/event/'.$event->id) }}">read more</a>
                    </div><!-- col-3 -->
                    @if($loop->iteration==4)
        </div>
        <div class="row topspace">
            @endif
            @endforeach
            @endisset
        </div><!-- row -->
    </div>
    <hr>
@endsection