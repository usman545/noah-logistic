@extends('layouts.frontend.app')
@section('content')
    <!-- <div id="parallax2" class="parallax">
        <div class="bg2 parallax-bg"></div>
        <div class="overlay"></div>
        <div class="parallax-content">
            <div class="container">


            </div>
        </div>
    </div> -->
    <div class="page_banner">
        @if(!empty($banner))
        <img src="{{asset('/uploads/banner/'.$banner->banner)}}" >
        @endif
    </div>

    <div class="breadcrumbs1_wrapper">
        <div class="container">
            <div class="breadcrumbs1"><a href="/">Home</a><span>/</span>Services</div>
        </div>
    </div>

    <div class="container">
        <h3>Our Services</h3>
        <hr style="margin-top: -12px;">
            @isset($services)

                @foreach($services as $service)
                  <div class="row">

                    <div class="col-md-4 col-sm-3 our-services">
                      <img src="{{asset('uploads/services/'.$service->picture)}}" alt="{{$service->title}}">

                   </div>
                  <div class="col-md-8">
                    <h4>{{ ($service->title) }}</h4>

                        <p>{!! (strlen($service->description)<=103)?$service->description:substr($service->description,0,180).'..' !!}</p>
                        <a href="{{ url('/service/'.$service->id) }}">read more</a>
                  </div><!-- col-3 -->
                </div>
                <hr>
        {{--            @if($loop->iteration==4)
        </div>
        <div class="row topspace">
        @endif --}}
            @endforeach
            @endisset
        </div><!-- row -->
@endsection
