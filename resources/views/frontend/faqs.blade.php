@extends('layouts.frontend.app')
@section('content')
  <style media="screen">

  </style>
  <div class="header-wrapper header-transparent">
          <!-- .header.header-style01 start -->
          <header id="header"  class="header-style01">
                @include('includes.topbar')

              <!-- .container start -->

                            @include('includes.menu')

          </header><!-- .header.header-style01 -->
      </div><!-- .header-wrapper.header-transparent end -->

      <!-- .page-title start -->
      <div class="page-title-style01 page-title-negative-top pt-bkg02">
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <h1>Noah's Ark Logistics</h1>

                      <div class="breadcrumb-container">
                          <ul class="breadcrumb clearfix">
                              <li>You are here:</li>

                              <li>
                                  <a href="{{ route('home')}}">Home</a>
                              </li>

                              <li>
                                  <a href="{{route('faqs')}}">faq</a>
                              </li>


                          </ul><!-- .breadcrumb end -->
                      </div><!-- .breadcrumb-container end -->
                  </div><!-- .col-md-7 end -->

              </div><!-- .row end -->
          </div><!-- .container end -->
      </div><!-- .page-title-style01.page-title-negative-top end -->


      <div class="page-content faqs">
          <div class="container">
              <div class="row">
                  <div class="col-md-8">
                      <div class="custom-heading">
                          <h2>Faqs</h2>
                      </div><!-- .custom-heading end -->

                          <div class="site_content">
                              <div class="row">
                              <div class="col-md-1 faq_num">1</div>
                              <div class="col-md-11 faq">
                                  <h3 class="faq_question">How do I apply?</h3>
                                   <div class="faq_answer">
                                   <p>
                                       You can apply online to PTCL after searching for openings/vacancies relevant to your area of interest.
                                       However, if there is no opening/vacancy, currently, to your area of interest, you can still leave your information with us by clicking on My Profile.
                                   </p>
                                   </div>
                              </div>
                            </div>

                            <div class="row">
                            <div class="col-md-1 faq_num">2</div>
                            <div class="col-md-11 faq">
                                <h3 class="faq_question">How do I apply?</h3>
                                 <div class="faq_answer">
                                 <p>
                                     You can apply online to PTCL after searching for openings/vacancies relevant to your area of interest.
                                     However, if there is no opening/vacancy, currently, to your area of interest, you can still leave your information with us by clicking on My Profile.
                                 </p>
                                 </div>
                            </div>
                          </div>

                          <div class="row">
                          <div class="col-md-1 faq_num">3</div>
                          <div class="col-md-11 faq">
                              <h3 class="faq_question">How do I apply?</h3>
                               <div class="faq_answer">
                               <p>
                                   You can apply online to PTCL after searching for openings/vacancies relevant to your area of interest.
                                   However, if there is no opening/vacancy, currently, to your area of interest, you can still leave your information with us by clicking on My Profile.
                               </p>
                               </div>
                          </div>
                        </div>
                      </div>

                  </div><!-- .container end -->
      </div><!-- .page-content end -->
</div>
</div>

@endsection
