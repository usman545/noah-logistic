@extends('layouts.frontend.app')
@section('content')

  <div class="header-wrapper header-transparent">
          <!-- .header.header-style01 start -->
          <header id="header"  class="header-style01">
                @include('includes.topbar')

              <!-- .container start -->

                            @include('includes.menu')

          </header><!-- .header.header-style01 -->
      </div><!-- .header-wrapper.header-transparent end -->

      <!-- .page-title start -->
      <div class="page-title-style01 page-title-negative-top pt-bkg02">
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <h1>Trucking - Global Logistics Solutions</h1>

                      <div class="breadcrumb-container">
                          <ul class="breadcrumb clearfix">
                              <li>You are here:</li>

                              <li>
                                  <a href="index.html">Home</a>
                              </li>

                              <li>
                                  <a href="about02.html">About</a>
                              </li>

                              <li>
                                  <a href="#">About Compact</a>
                              </li>
                          </ul><!-- .breadcrumb end -->
                      </div><!-- .breadcrumb-container end -->
                  </div><!-- .col-md-12 end -->
              </div><!-- .row end -->
          </div><!-- .container end -->
      </div><!-- .page-title-style01.page-title-negative-top end -->


      <div class="page-content">
          <div class="container">
              <div class="row">
                  <div class="col-md-6">
                      <div class="custom-heading">
                          <h2>company profile</h2>
                      </div><!-- .custom-heading end -->

                      <p>
                        We are a full service logistics company who takes care of your freight in the ocean,
on the ground, and in the air. The best solution and reliability are what sets us apart.
Our them of experience and dedicated people will handle your cargo so that you can
concentrate on your core business.
                      </p>

                  </div><!-- .col-md-6 end -->

                  <div class="col-md-6 animated triggerAnimation" data-animate="zoomIn">
                      <img src="{{asset('img/pics/img25.jpg')}}" alt=""/>
                  </div><!-- .col-md-6 end -->
              </div><!-- .row end -->
          </div><!-- .container end -->
      </div><!-- .page-content end -->

      <div class="page-content custom-bkg bkg-light-blue mb-70">
          <div class="container">
              <div class="row">
                  <div class="col-md-6">
                      <div class="custom-heading">
                          <h2>our mission</h2>
                      </div><!-- .custom-heading end -->

                      <p>
                        Our mission is to give the peace of mind to our clients by giving the most suitable
product and the best service. We will continually improve our services and product to
maintain and improve our edge.
                      </p>
                  </div><!-- .col-md-6 end -->

                  <div class="col-md-6">
                      <div class="custom-heading">
                          <h2>our promise</h2>
                      </div><!-- .custom-heading end -->

                      <ul class="fa-ul">
                          <li>
                              <i class="fa fa-li fa-long-arrow-right"></i>
                              Providing high quality transportation services to all of our clients
                          </li>

                          <li>
                              <i class="fa fa-li fa-long-arrow-right"></i>
                              Invest in our employees to provide better service and company growth
                          </li>

                          <li>
                              <i class="fa fa-li fa-long-arrow-right"></i>
                              Worry about enviroment according to latest industry standards
                          </li>

                          <li>
                              <i class="fa fa-li fa-long-arrow-right"></i>
                              Safety as top priority in assuring safe work procedures
                          </li>

                          <li>
                              <i class="fa fa-li fa-long-arrow-right"></i>
                              Investing in technology to provide fast, acurate and cost-effective service
                          </li>

                          <li>
                              <i class="fa fa-li fa-long-arrow-right"></i>
                              Living up to highest industry standards
                          </li>
                      </ul><!-- .fa-ul end -->
                  </div><!-- .col-md-6 end -->
              </div><!-- .row end -->
          </div><!-- .container end -->
      </div><!-- .page-content.custom-bkg end -->

      <div class="page-content">
          <div class="container">
              <div class="row">
                  <div class="custom-heading02">
                      <h2>Our core values</h2>
                      <p>THE THING THAT MAKES US DIFFERENT</p>
                  </div>
              </div><!-- .row end -->

              <div class="row">
                  <div class="col-md-3 col-sm-6">
                      <div class="service-icon-center">
                          <div class="icon-container">
                              <i class="fa fa-graduation-cap"></i>
                          </div>

                          <h4>Always learning</h4>

                          <p>
                              Cozy sphinx waves quart jug of bad milk. A
                              very bad quack might jinx zippy fowls. Few
                              quips galvanized the mock jury box.
                          </p>
                      </div><!-- .service-icon-center end -->
                  </div><!-- .col-md-3 end -->

                  <div class="col-md-3 col-sm-6">
                      <div class="service-icon-center">
                          <div class="icon-container">
                              <i class="fa fa-cogs"></i>
                          </div>

                          <h4>Latest Technology</h4>

                          <p>
                              Cozy sphinx waves quart jug of bad milk. A
                              very bad quack might jinx zippy fowls. Few
                              quips galvanized the mock jury box.
                          </p>
                      </div><!-- .service-icon-center end -->
                  </div><!-- .col-md-3 end -->

                  <div class="col-md-3 col-sm-6">
                      <div class="service-icon-center">
                          <div class="icon-container">
                              <i class="fa fa-cubes"></i>
                          </div>

                          <h4>Safety & Quality</h4>

                          <p>
                              Cozy sphinx waves quart jug of bad milk. A
                              very bad quack might jinx zippy fowls. Few
                              quips galvanized the mock jury box.
                          </p>
                      </div><!-- .service-icon-center end -->
                  </div><!-- .col-md-3 end -->

                  <div class="col-md-3 col-sm-6">
                      <div class="service-icon-center">
                          <div class="icon-container">
                              <i class="fa fa-tree"></i>
                          </div>

                          <h4>Care for Environment</h4>

                          <p>
                              Cozy sphinx waves quart jug of bad milk. A
                              very bad quack might jinx zippy fowls. Few
                              quips galvanized the mock jury box.
                          </p>
                      </div><!-- .service-icon-center end -->
                  </div><!-- .col-md-3 end -->
              </div><!-- .row end -->


          </div><!-- .container end -->
      </div><!-- .page-content end -->

      <div class="page-content parallax parallax01 dark">
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <div class="call-to-action clearfix">
                          <div class="text">
                              <h2>Our People and Culture.</h2>
                              <p>
                            Inquisitiveness, innovation, tenacity and love for our work is what defines us
                             and our work culture. We get the most satisfaction when our customers are satisfied.
                                 </p>
                          </div><!-- .text end -->

                          <a href="#" class="btn btn-big">
                              <span>get a quote</span>
                          </a>
                      </div><!-- .call-to-action end -->
                  </div><!-- .col-md-12 end -->
              </div><!-- .row end -->
          </div><!-- .container end -->
      </div><!-- .page-content.parallax end -->

@endsection
