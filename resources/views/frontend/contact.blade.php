@extends('layouts.frontend.app')
@section('content')
  <div class="header-wrapper header-transparent">
          <!-- .header.header-style01 start -->
          <header id="header"  class="header-style01">

                          @include('includes.menu')

          </header><!-- .header.header-style01 -->
      </div><!-- .header-wrapper.header-transparent end -->

      <!-- .page-title start -->
      <div class="page-title-map page-title-negative-top">
          <!-- .container start -->
          <div class="container-fluid">
              <div class="row">
                  <div id="map"></div>
              </div>
          </div><!-- .container end -->
      </div><!-- .page-title-style01.page-title-negative-top end -->

      <div class="page-content">
          <div class="container">
              <div class="row">
                  <div class="col-md-6">
                      <div class="custom-heading">
                          <h3>your inquiry</h3>
                      </div><!-- .custom-heading.left end -->

                      <p>
                  We have a network of third-party warehouses which gives us the flexibility to find the most suitable storage at the best price.    </p>

                      <br />

                      <div class="custom-heading">
                          <h4>company information</h4>
                      </div><!-- .custom-heading end -->

                      <address>
                        Noah's Ark Logistics<br>
                        G1-1111 Albion Rd, Etobicoke, <br>
                        ON M9V 1A9.
                      </address>

                      <span class="text-big colored">
                        416 619 4900
                      </span>
                      <br />

                      <a href="mailto:">info@noahsarklogistics.com</a>
                  </div><!-- .col-md-6 end -->

                  <div class="col-md-6 clearfix">
                      <!-- .contact form start -->
                      <form class="wpcf7">
                          <fieldset>
                              <label>
                                  <span class="required">*</span> Your request:
                              </label>

                              <select class="wpcf7-form-control-wrap wpcf7-select" id="contact-inquiry">
                                  <option value="I need an offer for contract logistics">I need an offer for contract logistics</option>
                                  <option value="I need an offer for air freight">I need an offer for air freight</option>
                                  <option value="I want to become your partner">I want to become your partner</option>
                                  <option value="I have some other request">I have some other request</option>
                              </select>
                          </fieldset>

                          <fieldset>
                              <label>
                                  <span class="required">*</span> First Name:
                              </label>

                              <input type="text" class="wpcf7-text" id="contact-name">
                          </fieldset>

                          <fieldset>
                              <label>
                                  <span class="required">*</span> Last Name:
                              </label>

                              <input type="text" class="wpcf7-text" id="contact-last-name">
                          </fieldset>

                          <fieldset>
                              <label>
                                  <span class="required">*</span> Email:
                              </label>

                              <input type="url" class="wpcf7-text" id="contact-email">
                          </fieldset>

                          <fieldset>
                              <label>
                                  <span class="required">*</span> Message:
                              </label>

                              <textarea rows="8" class="wpcf7-textarea" id="contact-message"></textarea>
                          </fieldset>

                          <input type="submit" class="wpcf7-submit" value="send" />
                      </form><!-- .wpcf7 end -->
                  </div><!-- .col-md-6 end -->
              </div><!-- .row end -->
          </div><!-- .container end -->
      </div><!-- .page-content end -->



@endsection
