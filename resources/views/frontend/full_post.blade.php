@extends('layouts.frontend.app')
@section('content')

  <div class="page_banner">
      @if(!empty($banner))
      <img src="{{asset('/uploads/banner/'.$banner->banner)}}" >
      @endif
  </div>

  <div class="breadcrumbs1_wrapper">
      <div class="container">
          <div class="breadcrumbs1"><a href="index.html">Home</a><span>/</span>About</div>
      </div>
  </div>



    <div id="content">
        <div class="container">

            <div class="row">

                <div class="col-sm-12">
                    <div class="blog_content">


                        <div class="post post-short">
                            <div class="post-header col-md-4">
                                <div class="post-slide ">
                                    <div id="sl1">
                                        <a class="sl1_prev" href="#"></a>
                                        <a class="sl1_next" href="#"></a>
                                        <div class="sl1_pagination"></div>
                                        <div class="carousel-box">
                                            <div class="inner">
                                                <div class="carousel main">
                                                    <ul>
                                                        <li>
                                                            <div class="sl1">
                                                                <div class="sl1_inner">
                                                                    <img src="{{asset('/uploads/'.$page.'/'.$picture)}}" alt=""
                                                                         class="img-responsive">
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-story col-md-8">
                                <h2>{{$page_title}}</h2>

                                {{--<div class="post-story-info">
                                    <div class="date1">April 16, 2016</div>
                                    <div class="by">Posted by <a href="#">George Smith</a> / <a href="#">26</a> Comments
                                    </div>
                                </div>--}}

                                <div class="post-story-body clearfix">
                                    {{--<div class="big_letter">L</div>--}}
                                    {!! $description !!}
                                </div>
                                {{--<div class="post-story-link">
                                    <a href="post.html" class="btn-default btn4">view post</a>
                                </div>--}}
                            </div>
                        </div>

{{--
                        <div class="post post-short">
                            <div class="post-header">
                                <div class="post-video">
                                    <iframe src="http://player.vimeo.com/video/123829510?portrait=0&byline=0&title=0&badge=0"
                                            width="500" height="281" frameborder="0" webkitallowfullscreen
                                            mozallowfullscreen allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="post-story">
                                <h2>Lorem ipsum dolor sit amet</h2>

                                <div class="post-story-info">
                                    <div class="date1">April 16, 2016</div>
                                    <div class="by">Posted by <a href="#">George Smith</a> / <a href="#">26</a> Comments
                                    </div>
                                </div>

                                <div class="post-story-body clearfix">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                        euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                        minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                        aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                                        vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla
                                        facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                        euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                        minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                        aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                                        vulputate velit esse molestie consequat, vel illum.
                                    </p>

                                </div>
                                <div class="post-story-link">
                                    <a href="post.html" class="btn-default btn4">view post</a>
                                </div>
                            </div>
                        </div>

                        <div class="pager_wrapper">
                            <ul class="pager clearfix">
                                <li class="prev"><a href="#">Previous</a></li>
                                <li class="li"><a href="#">1</a></li>
                                <li class="active"><a href="#">2</a></li>
                                <li class="li"><a href="#">3</a></li>
                                <li class="li"><a href="#">4</a></li>
                                <li class="li"><a href="#">5</a></li>
                                <li class="li"><a href="#">6</a></li>
                                <li class="li"><a href="#">7</a></li>
                                <li class="li"><a href="#">8</a></li>
                                <li class="li"><a href="#">9</a></li>
                                <li class="li"><a href="#">10</a></li>
                                <li class="next"><a href="#">Next</a></li>
                            </ul>
                        </div>
--}}


                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
