<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{


    public function index(){
      $data =
          [
              'page_title'  =>  'Noah LOGISTIC',
              'page'        =>  'home',
              'body_class'  =>  'front superslides-version'];
              return view('frontend.home', $data);


    }
    public function about(){
      $data =
          [
              'page_title'  =>  'About Noah',
              'page'        =>  'about',
              'body_class'  =>  'front about'];
              return view('frontend.about', $data);


    }

    public function contact(){
      $data =
          [
              'page_title'  =>  'Contact Us',
              'page'        =>  'contact',
              'body_class'  =>  'front contact'];
              return view('frontend.contact', $data);
}

public function news(){
  $data =
      [
          'page_title'  =>  'News',
          'page'        =>  'news',
          'body_class'  =>  'front news'];
          return view('frontend.news', $data);

}

public function services(){
  $data =
      [
          'page_title'  =>  'Services',
          'page'        =>  'services',
          'body_class'  =>  'front services'];
          return view('frontend.contact', $data);

}

public function privacy(){
  $data =
      [
          'page_title'  =>  'Privacy Policy',
          'page'        =>  'privacy',
          'body_class'  =>  'front privacy'];
          return view('frontend.privacy', $data);

}
public function locations(){
  $data =
      [
          'page_title'  =>  'Locations',
          'page'        =>  'locations',
          'body_class'  =>  'front privacy'];
          return view('frontend.locations', $data);

}

public function faqs(){
  $data =
      [
          'page_title'  =>  'Faqs',
          'page'        =>  'faqs',
          'body_class'  =>  'front faq'];
          return view('frontend.faqs', $data);

}


}
